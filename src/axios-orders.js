import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://burger-builder-22ba9.firebaseio.com/'
});

export default instance;