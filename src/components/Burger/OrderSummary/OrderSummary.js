import React from 'react';

import Button from '../../UI/Button/Button';
import Aux from '../../../hoc/Auxiliary/Auxiliary';

const orderSummary = (props) => {
  const ingredientsSummary = Object.keys(props.ingredients)
    .map(ingredient => {
      return (
        <li key={ingredient}>
          <span style={{ textTransform: 'capitalize' }}>{ingredient}</span>: {props.ingredients[ingredient]}
        </li>
      );
    });

  return (
    <Aux>
      <h3>Your Order</h3>
      <p>A delicious burger with the following ingredients:</p>
      <ul>
        {ingredientsSummary}
      </ul>
      <p><strong>Total Price: {props.price.toFixed(2)}</strong></p>
      <p>Continue to Checkout?</p>
      <Button btnType="Danger" clicked={props.purchaseCancelled}>CANCEL</Button>
      <Button btnType="Success" clicked={props.purchaseConfirmed}>CONTINUE</Button>
    </Aux>
  );
}

export default orderSummary;