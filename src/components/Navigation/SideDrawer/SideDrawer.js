import React from 'react';

import classes from './SideDrawer.css';
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import Aux from '../../../hoc/Auxiliary/Auxiliary';
import Backdrop from '../../UI/Backdrop/Backdrop';

const sideDrawer = (props) => {
  let attatchedClasses = [classes.SideDrawer, classes.Close];
  if (props.open) {
    attatchedClasses = [classes.SideDrawer, classes.Open];
  }

  return (
    <Aux>
      <Backdrop
        show={props.open}
        clicked={props.clicked} />
      <div className={attatchedClasses.join(' ')}>
        <div className={classes.Logo}>
          <Logo />
        </div>
        <nav>
          <NavigationItems />
        </nav>
      </div>
    </Aux>
  );
};

export default sideDrawer;